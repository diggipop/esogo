package de.codeyourapp.esogo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import de.codeyourapp.esogo.NonUI.POI;

public class AdapterPoiList extends BaseAdapter {

    Context context;
    ArrayList<POI> data;
    private static LayoutInflater inflater = null;

    public AdapterPoiList(Context context, ArrayList<POI> data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        final int position = pos;

        if (vi == null)
            vi = inflater.inflate(R.layout.content_ort, null);

        TextView text = vi.findViewById(R.id.text);
        text.setText(data.get(position).name);

        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent InfoIntent = new Intent(context, InfoActivity.class);
                InfoIntent.putExtra("PoiId", data.get(position).id);
                context.startActivity(InfoIntent);
            }
        });

        vi.findViewById(R.id.infoButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent InfoIntent = new Intent(context, InfoActivity.class);
                InfoIntent.putExtra("PoiId", data.get(position).id);
                context.startActivity(InfoIntent);
            }
        });

        return vi;
    }
}
