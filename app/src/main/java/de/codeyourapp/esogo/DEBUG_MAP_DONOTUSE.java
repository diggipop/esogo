package de.codeyourapp.esogo;


import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import de.codeyourapp.esogo.NonUI.OfflineGPS;

public class DEBUG_MAP_DONOTUSE extends AppCompatActivity implements OnMapReadyCallback {
    private MapView mapView;
    private ProgressBar spinner;
    private OfflineGPS gps;

    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug__map__donotuse);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        gps = new OfflineGPS(getApplicationContext(),this);


        mapView = findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);


        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
/*
        Button button = findViewById(R.id.TestButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //DEBUG 50.9999543,11.0486513
                LatLng currentPos = new LatLng(50.9999543,11.0486513);
                MapWrapper.SetCurrentPos(currentPos);
                MapWrapper.DrawRoute(
                        new ArrayList(pointList),
                        new Runnable() {
                            @Override
                            public void run(){ spinner.setVisibility(View.VISIBLE); }
                        },
                        new Runnable() {
                            @Override
                            public void run(){ spinner.setVisibility(View.GONE); }
                        });
            }
        });*/
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    private ArrayList<LatLng> pointList = new ArrayList<LatLng>();
    private GoogleMap gmap;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;

        pointList.add(new LatLng(51.0213225, 11.0259966));
        pointList.add(new LatLng(50.9761553, 11.0194259));
        pointList.add(new LatLng(50.9774515, 10.9596516));
/*
        MapWrapper.setAPIKey("5b3ce3597851110001cf6248e97ab34730e047bdbaa6b1d16e46a115");
        MapWrapper.setTheMap(gmap);
        MapWrapper.setTheContext(getApplicationContext());
        MapWrapper.DrawPoints(pointList);
        MapWrapper.FokusPoints(pointList);
*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}