package de.codeyourapp.esogo;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Locale;

import de.codeyourapp.esogo.NonUI.POI;
import de.codeyourapp.esogo.NonUI.RouteWrapper;

public class InfoActivity extends AppCompatActivity {
    POI myPoi = null;
    ProgressBar spinner = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            myPoi = RouteWrapper.GetPOIById(extras.getInt("PoiId"));
            Log.i("MyDebug","InfoActivity: id is " + myPoi.id);
        }

        if(myPoi != null) {
            TextView textView = findViewById(R.id.infoText);

            if( Locale.getDefault().getISO3Language().equals("deu")) {
                textView.setText(myPoi.infoDE);
            } else {
                textView.setText(myPoi.info);
            }

            new DownloadImageTask().execute(myPoi.picUrl);
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Drawable> {
        protected Drawable doInBackground(String... urls) {
            return RouteWrapper.LoadImageFromWebOperations(myPoi.picUrl);
        }

        protected void onProgressUpdate() {

        }

        protected void onPreExecute() {
            spinner.setVisibility(View.VISIBLE);
        }

        protected void onPostExecute(Drawable result) {
            ImageView imageView = findViewById(R.id.imageView3);
            spinner.setVisibility(View.GONE);
            imageView.setImageDrawable(result);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_impressum:
                Intent ImpressumIntent = new Intent(this, Impressum.class);
                startActivity(ImpressumIntent);
                return true;

            case R.id.action_gallery:
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setType("image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;

            case R.id.action_exit:
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
