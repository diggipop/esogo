package de.codeyourapp.esogo.NonUI;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class APIConnector {
    public static JSONObject ContactAPI(String strUrl) {
        String data = "";
        JSONObject json = null;

        Log.i("MyDebug","Contacting API with: " + strUrl);

        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            json = new JSONObject(data);

            br.close();
            iStream.close();
        } catch (MalformedURLException e) {
            Log.e("Exception", "MalformedURLException : " + e.getStackTrace());
        } catch (IOException e) {
            Log.e("Exception", "IOException : " + e.getStackTrace());
        } catch (JSONException e) {
            Log.e("Exception", "JSONException : " + e.getMessage());
        } finally {
            urlConnection.disconnect();
        }

        //Log.i("MyDebug","API responded with: " + json.toString());

        return json;
    }
}
