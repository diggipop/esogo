package de.codeyourapp.esogo.NonUI;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.Marker;

import java.util.List;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import de.codeyourapp.esogo.R;

public class MapWrapper {
    // Eigenschaften
    private static final MapWrapper ourInstance = new MapWrapper();
    public static MapWrapper getInstance() {
        return ourInstance;
    }

    private static GoogleMap theMap;
    public static void setTheMap(GoogleMap theMap) {
        MapWrapper.theMap = theMap;
    }

    private static Context theContext;
    public static void setTheContext(Context theContext) {
        MapWrapper.theContext = theContext;
    }

    private static String APIKey;
    public static void setAPIKey(String key) {
        MapWrapper.APIKey = key;
    }

    private static boolean isRunningTasks = false;

    private static Marker currentPosMarker = null;
    public static LatLng currentPos = null;
    private static List<Marker> markers = new ArrayList<Marker>();
    private static List<Polyline> lines = new ArrayList<Polyline>();
    public static Route currentRoute = null;



    // Öffentliche Methoden
    public static void FokusRoute() {
        ArrayList<LatLng> list = new ArrayList<LatLng>();

        if(currentRoute != null) {
            for (POI poi : currentRoute.pois) {
                list.add(poi.pos);
            }
        }

        if(currentPos != null) {
            if(! (currentPos.latitude == 0 && currentPos.longitude == 0)) {
                list.add(0, currentPos);
            }
        }

        if(theMap != null) {
            LatLngBounds bound = CalcBound(list);
            theMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bound, 50) );
        }
    }

    public static void FokusPoints(ArrayList<POI> points) {
        ArrayList<LatLng> list = new ArrayList<LatLng>();

        for (POI poi : points) {
            list.add(poi.pos);
        }

        if(currentPos != null) {
            list.add(0, currentPos);
        }

        LatLngBounds bound = CalcBound(list);

        //TODO: Zentrum intelligent bestimmten (Um alle Punkte auf der Karte zu sehen)
        theMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bound, 50) );
    }

    public static void DrawPoints(ArrayList<POI> points) {
        // Alte Marker entfernen
        for (Marker marker : markers) {
            marker.remove();
        }

        // Neue Marker zeichnen
        for(POI point : points) {
            MarkerOptions options = new MarkerOptions();
            options.position(point.pos);
            markers.add(theMap.addMarker(options));
        }

        FokusRoute();
    }

    public static void DrawRoute(List<POI> points, LatLng endPos,Runnable PreCallback,Runnable PostCallback) {
        if(isRunningTasks) {
            return;
        }

        currentRoute.lenght = 0;
        currentRoute.durration = 0;
        ourInstance.drawRoute(points,endPos,PreCallback,PostCallback);
    }

    public static void SetCurrentPos(LatLng currentPos) {
        if(currentPos.longitude == 0 && currentPos.latitude == 0) {
            MapWrapper.currentPos = null;
        } else {
            MapWrapper.currentPos = currentPos;
        }

        // evtl vorhandenen Marker entfernen
        if(currentPosMarker != null) {
            currentPosMarker.remove();
            currentPosMarker = null;
        }

        MarkerOptions option = new MarkerOptions().position(currentPos);

        if(theMap != null)  {
            option.title("Current Pos");
            option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_crosshair));
            currentPosMarker = theMap.addMarker(option);
        }

        FokusRoute();
    }

    private static LatLngBounds CalcBound(List<LatLng> points) {
        Double minLat = 9999999.0;
        Double maxLat = 0.0;
        Double minLng = 9999999.0;
        Double maxLng = 0.0;

        for(LatLng pos : points) {
            if(pos.latitude < minLat) {
                minLat = pos.latitude;
            }
            if(pos.latitude > maxLat){
                maxLat= pos.latitude;
            }
            if(pos.longitude < minLng){
                minLng= pos.longitude;
            }
            if(pos.longitude> maxLng){
                maxLng=pos.longitude;
            }

        }
        LatLng minPos = new LatLng(minLat,minLng);
        LatLng maxPos = new LatLng(maxLat,maxLng);
        return new LatLngBounds(minPos,maxPos);
    }

    // internal methods / classes
    private class RouteSettings {
        public LatLng endPos;
        public List<LatLng> points;
        public List<PolylineOptions> lines;
        public Runnable PreCallback;
        public Runnable PostCallback;

        RouteSettings(List<POI> points, LatLng endPos, Runnable preCallback, Runnable postCallback) {
            ArrayList<LatLng> list = new ArrayList<LatLng>();

            for (POI poi : points) {
                list.add(poi.pos);
            }

            this.points = list;
            this.endPos = endPos;
            this.lines = new ArrayList<PolylineOptions>();
            this.PreCallback = preCallback;
            this.PostCallback = postCallback;
        }
    }

    private void drawRoute(List<POI> points, LatLng endPos,Runnable PreCallback,Runnable PostCallback) {
        new GetRouteTask(new RouteSettings(points, endPos, PreCallback, PostCallback)).execute();
    }

    private class GetRouteTask extends AsyncTask<RouteSettings,Void,Void> {
        private RouteSettings settings;

        public GetRouteTask(RouteSettings settings) {
            super();
            this.settings = settings;
        }

        @Override
        protected Void doInBackground(RouteSettings... routeSettings) {

            if(currentPos != null) {
                if (! (currentPos.latitude == 0 && currentPos.longitude == 0)) {
                   settings.points.add(0, currentPos);
                }
            }

            if(settings.endPos !=  null) {
                if(! (settings.endPos.latitude == 0 && settings.endPos.longitude == 0)) {
                    settings.points.add(settings.endPos);
                }
            }

            if(settings.points.size() < 2) {
                //DebugNotifier.showMessage(theContext,"Error: Route besteht aus weniger als 2 Punkten");
                return null;
            }

            // Route für jeweils zwei Punkte, zb 3 Punkte -> 2 Routen
            Log.i("DEBUG3","routes" + settings.points.size());
            for(int c = 0;c < settings.points.size() -1; c++) {
                int color = (c == 0) ? Color.BLUE : Color.RED;

                Log.e("MyDebug","color:" + color);

                PolylineOptions line = GetLine(settings.points.get(c),settings.points.get(c+1), color, (c == 0) ? 12 : 8);
                settings.lines.add(line);
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            settings.PreCallback.run();
            isRunningTasks = true;
        }

        @Override
        protected void onPostExecute(Void result) {
            // alte Route entfernen
            for (Polyline line : lines) {
                line.remove();
            }

            // neue Route zeichnen
            for (PolylineOptions line : settings.lines) {
                lines.add(theMap.addPolyline(line));
            }

            settings.PostCallback.run();
            isRunningTasks = false;
        }
    }

    private PolylineOptions GetLine(LatLng startPos, LatLng endPos, int color, int width) {
        JSONObject response = getDirectionsFromAPI(startPos, endPos);
        List<LatLng> route = ParseJSONToRoute(response);

        currentRoute.lenght += GetRouteLenghtFromJSON(response);
        currentRoute.durration += GetRouteDurrationFromJSON(response);

        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.addAll(route);
        lineOptions.width(width);
        lineOptions.color(color);
        lineOptions.geodesic(true);

        return lineOptions;
    }

    private static List<LatLng> ParseJSONToRoute(JSONObject json) {
        List<LatLng> points = new ArrayList<LatLng>();

        try {
            JSONArray coords = json.getJSONArray("features").getJSONObject(0).getJSONObject("geometry").getJSONArray("coordinates");
            Log.i("DEBUG3",coords.length() + " coords");

            for(int i=0;i < coords.length();i++) {
                JSONArray point = coords.getJSONArray(i);
                points.add(new LatLng(point.getDouble(1),point.getDouble(0)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return points;
    }

    private static double GetRouteLenghtFromJSON(JSONObject json) {
        double lenght = 0;
        try {
            lenght = json.getJSONArray("features").getJSONObject(0).getJSONObject("properties").getJSONObject("summary").getDouble("distance");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lenght;
    }

    private static double GetRouteDurrationFromJSON(JSONObject json) {
        double dur = 0;
        try {
            dur = json.getJSONArray("features").getJSONObject(0).getJSONObject("properties").getJSONObject("summary").getDouble("duration");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dur;
    }

    private static JSONObject getDirectionsFromAPI(LatLng startPos, LatLng endPos) {
        String strKey = "api_key=" + APIKey;
        String strProfile = "foot-walking";
        String strCoords = "start=" + startPos.longitude + "," + startPos.latitude + "&end=" + endPos.longitude + "," + endPos.latitude;
        String strUrl = "https://api.openrouteservice.org/v2/directions/" + strProfile + "?" + strKey + "&" + strCoords;




        JSONObject response = new JSONObject();
        Log.i("DEBUG2",strUrl);

        try {
            response = APIConnector.ContactAPI(strUrl);
        } catch (Exception ex) {
            //DebugNotifier.showMessage(theContext,"Error: API nicht erreichbar :/");
            //Log.e("DEBUG2", ex.toString());
        }

        return response;
    }

}
