package de.codeyourapp.esogo.NonUI;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;

import de.codeyourapp.esogo.NonUI.MapWrapper;


public class OfflineGPS implements LocationListener {
    public static OfflineGPS myInstance;
    Context mContext;
    LocationManager locationManager;

    String locationText = "";
    double locationLatitude = 0;
    double locationLongitude = 0;

    /*
    Todo: Translation
    */
    final String _enableGPS = "Bitte GPS aktivieren";

    private int mInterval = 3000; // 3 seconds by default, can be changed later
    private Handler mHandler;

    /**
     *
     * @param mContext  get from Initialisation
     * @param mactivity get from Initialisation
     */
    public OfflineGPS (Context mContext, Activity mactivity) {
        myInstance = this;
        this.mContext = mContext;
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            public void run() {
                Log.i("handler2","run");
                mHandler = new Handler();
                startRepeatingTask();
            }
        }, 5000);   //5 seconds
    }

    /**
     *
     * @return
     */
    public String getLocationText() {
        return locationText;
    }
    /**
     *
     * @return
     */
    public double getLongitude() {
        return locationLongitude;
    }
    /**
     *
     * @return
     */
    public double getLatitude() {
        return locationLatitude;
    }

    /**
     * Runnable Routine
     */
    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                getLocation(); //this function can change value of mInterval.
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            finally {
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    void startRepeatingTask() {
        Log.i("startRepeatingTask","->");
        mStatusChecker.run();
    }

    /**
     * STOP Service
     */
    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }

    /**
     *
     */
    void getLocation() {
        Log.i("getLocation","");

        try {

            locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    2000, 5, (LocationListener) this);

            if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                locationLatitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
                locationLongitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
            }

            /*  Hier ist der Pull  !!!*/
            MapWrapper.SetCurrentPos(new LatLng(locationLatitude,locationLongitude));

        } catch (SecurityException e) {
            Log.i("???","SecurityException");
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        locationText = location.getLatitude() + "," + location.getLongitude();
        locationLatitude = location.getLatitude();
        locationLongitude = location.getLongitude();
    }

    @Override
    public void onProviderDisabled(String provider) {
        LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE );
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (statusOfGPS == false) {
            Toast.makeText(mContext, _enableGPS, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, _enableGPS, Toast.LENGTH_SHORT).cancel();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }
}