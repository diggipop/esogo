package de.codeyourapp.esogo.NonUI;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class POI {
    public int id;
    public String name;
    public String info;
    public String infoDE;
    public String picUrl = "https://esogo.ewelt.net/pics/testpic.jpg";
    public LatLng pos;

    POI(int id, String name, String info, String infoDE, LatLng pos) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.infoDE = infoDE;
        this.pos = pos;
    }

    POI(int id, String name, String info, String infoDE, LatLng pos, String picUrl) {
        this.id = id;
        this.name = name;
        this.info = info;
        this.infoDE = infoDE;
        this.pos = pos;
        this.picUrl = picUrl;
    }
}
