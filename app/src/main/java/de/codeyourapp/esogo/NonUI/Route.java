package de.codeyourapp.esogo.NonUI;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class Route {
    public int id;
    public String name;
    public ArrayList<POI> pois;
    public LatLng finsh;
    public double lenght;
    public double durration;

    public  ArrayList<Integer> skippedPoiIds = new ArrayList<Integer>();

    Route(int id, String name, ArrayList<POI> list) {
        this.id = id;
        this.name = name;
        this.pois = list;
    }

    public ArrayList<POI> getRemainingPois() {
        ArrayList<POI> list = new ArrayList<POI>();

        for(POI poi : pois) {
            if(! skippedPoiIds.contains(poi.id)) {
                list.add(poi);
            }
        }

        return list;
    }

    public void SkipNearstPoi(LatLng point) {
        double smallestDistance = 99999999999.0;
        int nearestId = 0;

        if(point == null) {
            return;
        }

        if(point.longitude == 0 && point.latitude == 0) {
            return;
        }

        for(POI poi : getRemainingPois()) {
            double lat = poi.pos.latitude - point.latitude;
            double lng = poi.pos.longitude - point.longitude;
            double distance = Math.sqrt((lat * lat) + (lng * lng));

            if(distance < smallestDistance) {
                smallestDistance = distance;
                nearestId = poi.id;
            }
        }

        if( (skippedPoiIds.size() + 2) < pois.size()) {
            skippedPoiIds.add(nearestId);
        }
    }
}
