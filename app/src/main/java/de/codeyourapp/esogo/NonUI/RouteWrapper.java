package de.codeyourapp.esogo.NonUI;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RouteWrapper {
    private static final RouteWrapper ourInstance = new RouteWrapper();
    public static RouteWrapper getInstance() {
        return ourInstance;
    }

    private static boolean isRunningTasks = false;
    private static ArrayList<Route> knownRouts = new ArrayList<Route>();
    private static ArrayList<POI> knownPois = new ArrayList<POI>();

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            Log.i("MyDebug","Downloading " + url);
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (MalformedURLException e) {
            Log.e("MyDebug","MalformedURLException downloading " + url + " - "+ e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("MyDebug","IOException downloading " + url + " - "+ e.getMessage());
            return null;
        }
    }

    public static POI GetPOIById(int id) {
        for(POI poi : knownPois) {
            if(poi.id == id)
                return poi;
        }

        //TODO aus API laden, wenn nicht lokal verfügbar
        return null;
    }

    public static Route GetRouteById(int id) {
        for(Route route : knownRouts) {
            if(route.id == id)
                return route;
        }

        //TODO aus API laden, wenn nicht lokal verfügbar
        return null;
    }

    public static void GetAllRoutsFromAPI(Runnable PreCallback,MyRunnable PostCallback) {
        if(isRunningTasks) {
            return;
        }

        ourInstance.getAllRoutsFromAPI(PreCallback,PostCallback);
    }

    private void getAllRoutsFromAPI(Runnable PreCallback, MyRunnable PostCallback) {
        new RouteWrapper.GetRoutesTask(new RoutesSettings(PreCallback, PostCallback)).execute();
    }

    private class RoutesSettings {
        public List<Route> routes;
        public Runnable PreCallback;
        public MyRunnable PostCallback;

        RoutesSettings(Runnable preCallback, MyRunnable postCallback) {
            this.routes = new ArrayList<Route>();
            this.PreCallback = preCallback;
            this.PostCallback = postCallback;
        }
    }

    private class GetRoutesTask extends AsyncTask<RoutesSettings,Void,ArrayList<Route>> {
        private RoutesSettings settings;

        public GetRoutesTask(RoutesSettings settings) {
            super();
            this.settings = settings;
        }

        @Override
        protected ArrayList<Route> doInBackground(RoutesSettings... settings) {
            ArrayList<Route> response = new ArrayList<Route>();

            JSONObject json = APIConnector.ContactAPI("https://esogo.ewelt.net/db_connect/esogodb.php?authkey=test321&type=route&id=0");
            //JSONObject json = APIConnector.ContactAPI("https://esogo.ewelt.net/test.php");

            try {
                knownRouts.clear();
                knownPois.clear();
                JSONArray routs = json.getJSONArray("routs");

                // Finde alle POIs
                for(int i=0;i < routs.length();i++) {
                    Log.i("MyDebug","Route: " + (i+1));

                    JSONObject routeJ = routs.getJSONObject(i);
                    ArrayList<POI> poiList = new ArrayList<POI>();
                    Route route = new Route((i+1), routeJ.getString("name"), poiList);

                    JSONArray pois = routs.getJSONObject(i).getJSONArray("poi");

                    Log.i("MyDebug","Found " + (pois.length()) + " POIs");
                    for(int j=0;j < pois.length();j++) {
                        JSONObject poiJ = pois.getJSONObject(j);
                        POI poi = RouteWrapper.GetPOIById(poiJ.getInt("id"));

                        if(poi == null) {
                            Log.i("MyDebug","POI( "+poiJ.getInt("id")+" ) not in list...");
                            poi = new POI(
                                    poiJ.getInt("id"),
                                    poiJ.getString("Ort"),
                                    poiJ.getString("Englisch"),
                                    poiJ.getString("Deutsch"),
                                    new LatLng(poiJ.getDouble("lat"),poiJ.getDouble("lng")),
                                    poiJ.getString("Pic")
                            );
                            knownPois.add(poi);
                        } else {
                            Log.i("MyDebug","POI( "+poiJ.getInt("id")+" ) allready known in list...");
                        }
                        poiList.add(poi);
                    }


                    response.add(route);
                    knownRouts.add(route);
                }
                Log.i("MyDebug","Found " + (knownRouts.size()) + " unique Routs");
                Log.i("MyDebug","Found " + (knownPois.size()) + " unique POIs");

            } catch (JSONException e) {
                Log.i("Execption","GetRoutesTask -> " + e.getMessage());
            }

            return response;
        }

        @Override
        protected void onPreExecute() {
            settings.PreCallback.run();
            isRunningTasks = true;
        }

        @Override
        protected void onPostExecute(ArrayList<Route> result) {
            settings.PostCallback.run(result);
            isRunningTasks = false;
        }
    }

}
