package de.codeyourapp.esogo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import de.codeyourapp.esogo.NonUI.Route;
import de.codeyourapp.esogo.NonUI.RouteWrapper;

public class OrtActivity extends AppCompatActivity {
    private Route myRoute = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ort);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            myRoute = RouteWrapper.GetRouteById(extras.getInt("RouteId"));
        }

        Button startRouteButton = findViewById(R.id.startRouteButton);
        startRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent RouteplanenIntent = new Intent(OrtActivity.this, RouteActivity.class);
                RouteplanenIntent.putExtra("RouteId",myRoute.id);
                startActivity(RouteplanenIntent);
            }
        });


        ListView listview = findViewById(R.id.testList);
        listview.setAdapter(new AdapterPoiList(this, myRoute.pois ));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_impressum:
                Intent ImpressumIntent = new Intent(this, Impressum.class);
                startActivity(ImpressumIntent);
                return true;

            case R.id.action_gallery:
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setType("image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;

            case R.id.action_exit:
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
