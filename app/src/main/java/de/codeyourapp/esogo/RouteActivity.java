package de.codeyourapp.esogo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.codeyourapp.esogo.NonUI.MapWrapper;
import de.codeyourapp.esogo.NonUI.OfflineGPS;
import de.codeyourapp.esogo.NonUI.Route;
import de.codeyourapp.esogo.NonUI.RouteWrapper;

public class RouteActivity extends AppCompatActivity implements OnMapReadyCallback {

    private ArrayList<LatLng> pointList = new ArrayList<LatLng>();
    private GoogleMap gmap;
    private MapView mapView;
    private ProgressBar spinner;
    private OfflineGPS gps;
    private Route myRoute;

    private TextView lenghtText;
    private TextView durrationText;

    // camera implementation
    static final int REQUEST_PICTURE_CAPTURE = 1;
    static final int MY_PERMISSIONS_REQUEST_READ_PICTURE = 1888;
    static final int REQUEST_CAMERA = 1234;
    //private ImageView image;
    private String pictureFilePath;
    ////////////////

    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            myRoute = RouteWrapper.GetRouteById(extras.getInt("RouteId"));
            myRoute.skippedPoiIds.clear();
        }

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);

        lenghtText = findViewById(R.id.textViewLenght);
        durrationText = findViewById(R.id.textViewDuration);

        Button nextButton = findViewById(R.id.NextPointButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(MapWrapper.currentPos == null) {
                            Log.e("MyDebug","curPos == null");
                            return;
                        }

                        if(MapWrapper.currentPos.longitude == 0 && MapWrapper.currentPos.latitude == 0) {
                            Log.e("MyDebug","curPos long and lat == 0");
                            return;
                        }

                        myRoute.SkipNearstPoi(MapWrapper.currentPos);
                        MapWrapper.DrawPoints(myRoute.getRemainingPois());
                        MapWrapper.DrawRoute(
                                myRoute.getRemainingPois(),
                                myRoute.finsh,
                                new Runnable() {
                                    @Override
                                    public void run(){ spinner.setVisibility(View.VISIBLE); }
                                },
                                new Runnable() {
                                    @Override
                                    public void run(){
                                        spinner.setVisibility(View.GONE);
                                        lenghtText.setText(String.format("%.2f km", myRoute.lenght / 1000));
                                        durrationText.setText(String.format("%.2f min", myRoute.durration / 60));
                                    }
                                });
                    }
                });

        mapView = findViewById(R.id.mapView);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);
        MapWrapper.currentRoute = myRoute;

        // Camera implementation code

        ImageButton captureButton = findViewById(R.id.capture);
        captureButton.setOnClickListener(capture);

        if(!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            captureButton.setEnabled(false);
        }

        //granting permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_PICTURE);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;

        MapWrapper.setAPIKey("5b3ce3597851110001cf6248e97ab34730e047bdbaa6b1d16e46a115");
        MapWrapper.setTheMap(gmap);
        MapWrapper.setTheContext(getApplicationContext());

        gmap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLng curPos = new LatLng(OfflineGPS.myInstance.getLatitude(),OfflineGPS.myInstance.getLongitude());
                myRoute.finsh = curPos;

                MapWrapper.DrawPoints(myRoute.pois);
                MapWrapper.DrawRoute(
                        myRoute.pois,
                        curPos,
                        new Runnable() {
                            @Override
                            public void run(){ spinner.setVisibility(View.VISIBLE); }
                        },
                        new Runnable() {
                            @Override
                            public void run(){
                                lenghtText.setText(String.format("%.2f km", myRoute.lenght / 1000));
                                durrationText.setText(String.format("%.2f min", myRoute.durration / 60));
                                spinner.setVisibility(View.GONE);
                            }
                        });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }
    @Override
    protected void onPause() {
        mapView.onPause();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    // camera implementation

    private View.OnClickListener capture = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void onClick(View view) {

     /*       if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.CAMERA)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed; request the permission
                    requestPermissions(
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CAMERA);
                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                sendTakePictureIntent();
            }  */

            sendTakePictureIntent();
        }

    };

  /*  @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                sendTakePictureIntent();
                Log.i("TAG", "CAMERA permission has now been granted. Showing preview.");
            } else {
                Log.i("TAG", "CAMERA permission was NOT granted.");
            }
            // END_INCLUDE(permission_result)

        } else if (requestCode == MY_PERMISSIONS_REQUEST_READ_PICTURE) {
            Log.i("TAG", "Received response for contact permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                Log.i("TAG", "STORAGE permission has now been granted. Showing preview.");
            } else {
                Log.i("TAG", "STORAGE permission was NOT granted.");
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }  */

    private void sendTakePictureIntent() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra( MediaStore.EXTRA_FINISH_ON_COMPLETION, true);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {

            File pictureFile = null;
            try {
                pictureFile = getPictureFile();
            } catch (IOException ex) {
                Toast.makeText(this,
                        "Photo file can't be created, please try again, or check Camera and Storage permissions",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            if (pictureFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "de.codeyourapp.android.fileprovider",
                        pictureFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, REQUEST_PICTURE_CAPTURE);
            }
        }
    }

    private File getPictureFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String pictureFile = "StepMap_" + timeStamp;
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File image = File.createTempFile(pictureFile,  ".jpg", storageDir);
        pictureFilePath = image.getAbsolutePath();

        return image;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PICTURE_CAPTURE && resultCode == RESULT_OK) {
            File imgFile = new  File(pictureFilePath);
            if(imgFile.exists())            {
                //image.setImageURI(Uri.fromFile(imgFile));
                addToGallery();
            }
        }
    }
    //save captured picture in gallery
    private View.OnClickListener saveGallery = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            addToGallery();
        }
    };
    private void addToGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(pictureFilePath);
        Uri picUri = Uri.fromFile(f);
        galleryIntent.setData(picUri);
        this.sendBroadcast(galleryIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_impressum:
                Intent ImpressumIntent = new Intent(this, Impressum.class);
                startActivity(ImpressumIntent);
                return true;

            case R.id.action_gallery:
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setType("image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;

            case R.id.action_exit:
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
