package de.codeyourapp.esogo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;

import de.codeyourapp.esogo.NonUI.MyRunnable;
import de.codeyourapp.esogo.NonUI.Route;
import de.codeyourapp.esogo.NonUI.RouteWrapper;

public class RouteplanenActivity extends AppCompatActivity {
    RouteplanenActivity thisActivity;
    private ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routeplanen);

        spinner = findViewById(R.id.progressBar);
        spinner.setVisibility(View.GONE);
        thisActivity = this;

        RouteWrapper.GetAllRoutsFromAPI(
                new Runnable() {
                    @Override
                    public void run(){ spinner.setVisibility(View.VISIBLE); }
                },

                new MyRunnable() {
                    @Override
                    public void run(Object result) {
                        ArrayList<Route> routs = (ArrayList<Route>) result;
                        ListView listview = findViewById(R.id.routList);
                        listview.setAdapter(new AdapterRouteList (thisActivity, routs));
                        spinner.setVisibility(View.GONE);
                    }
                }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_impressum:
                Intent ImpressumIntent = new Intent(this, Impressum.class);
                startActivity(ImpressumIntent);
                return true;

            case R.id.action_gallery:
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setType("image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;

            case R.id.action_exit:
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
